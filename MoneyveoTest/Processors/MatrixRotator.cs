﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MoneyveoTest.Processors
{
    public class MatrixRotator
    {
        public int[,] Rotate(int[,] matrix)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1))
                throw new ArgumentException("Array is not a matrix");

            int maxIndex = matrix.GetLength(0) - 1;

            for (int i = 0; i <= maxIndex / 2; i++)
            {
                MatrixCircle circle = new MatrixCircle(matrix, i, maxIndex);
                ThreadPool.QueueUserWorkItem(circle.Run);
            }
            return matrix;
        }

        class MatrixCircle
        {
            private readonly int[,] matrix;

            private readonly int i;

            private readonly int maxIndex;

            public MatrixCircle(int[,] matrix, int i, int maxIndex)
            {
                this.matrix = matrix;
                this.i = i;
                this.maxIndex = maxIndex;
            }

            public void Run(object state)
            {
                for (int j = i; j < maxIndex - i; j++)
                {
                    var temp = matrix[i, j];
                    matrix[i, j] = matrix[maxIndex - j, i];
                    matrix[maxIndex - j, i] = matrix[maxIndex - i, maxIndex - j];
                    matrix[maxIndex - i, maxIndex - j] = matrix[j, maxIndex - i];
                    matrix[j, maxIndex - i] = temp;
                }
            }
        }
    }
}
