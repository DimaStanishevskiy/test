﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyveoTest.Processors
{
    public class CsvReader
    {
        public async Task<int[,]> ReadAsync(string filePath)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string csv = await reader.ReadToEndAsync();

                return Convert(csv);
            }
        }

        private int[,] Convert(string csvString)
        {
            var stringMatrix = csvString.Trim().Split('\n').Select(x => x.Split(','));

            if (stringMatrix.Any(x => x.Count() != stringMatrix.Count()))
            {
                throw new Exception("File is not a matrix");
            }

            var matrix = new int[stringMatrix.Count(), stringMatrix.Count()];

            int i = 0;
            foreach (var row in stringMatrix)
            {
                int j = 0;
                foreach (var value in row)
                {
                    try
                    {
                        matrix[i, j] = System.Convert.ToInt32(value);
                    }
                    catch (InvalidCastException ex)
                    {
                        throw new Exception("File is not a matrix", ex);
                    }
                    j++;
                }
                i++;
            }
            return matrix;
        }
    }
}
