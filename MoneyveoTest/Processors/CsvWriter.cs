﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyveoTest.Processors
{
    public class CsvWriter
    {
        public async Task WriteAsync(string filePath, int[,] matrix)
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                await writer.WriteAsync(Convert(matrix));
            }
        }

        private string Convert(int[,] matrix)
        {
            string csv = "";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    csv += $"{matrix[i, j]}";
                    if (j != matrix.GetLength(1) - 1)
                        csv += ',';
                }
                if (i != matrix.GetLength(0) - 1)
                    csv += '\n';
            }
            return csv;
        }
    }
}
