﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyveoTest.Processors
{
    public class MatrixGenerator
    {
        public int[,] Generate(int size)
        {
            if (size < 0)
                throw new ArgumentException("Size can not be less than 0");
            int[,] matrix = new int[size, size];
            Random random = new Random();

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    matrix[i, j] = random.Next(10);

            return matrix;
        }
    }
}
