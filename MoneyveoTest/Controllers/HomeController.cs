﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyveoTest.Models;
using MoneyveoTest.Processors;

namespace MoneyveoTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly MatrixGenerator _matrixGenerator;
        private readonly MatrixRotator _matrixRotator;
        private readonly CsvReader _csvReader;
        private readonly CsvWriter _csvWriter;

        public HomeController(
            MatrixGenerator matrixGenerator,
            MatrixRotator matrixRotator,
            CsvReader csvReader,
            CsvWriter csvWriter
            )
        {
            _matrixGenerator = matrixGenerator;
            _matrixRotator = matrixRotator;
            _csvReader = csvReader;
            _csvWriter = csvWriter;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Rotate([FromBody] int[,] matrix)
        {
            matrix = _matrixRotator.Rotate(matrix);

            return Ok(matrix);
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] int[,] matrix)
        {
            await _csvWriter.WriteAsync("test.scv", matrix);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Load()
        {
            var matrix = await _csvReader.ReadAsync("test.scv");
            return Ok(matrix);
        }

        [HttpPost]
        public async Task<IActionResult> Random([FromBody]int size)
        {
            var matrix = _matrixGenerator.Generate(size);
            return Ok(matrix);
        }
    }
}
