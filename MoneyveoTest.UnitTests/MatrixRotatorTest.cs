﻿using MoneyveoTest.Processors;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MoneyveoTest.UnitTests
{
    public class MatrixRotatorTest
    {
        [Fact]
        public void Rotate()
        {
            MatrixRotator rotator = new MatrixRotator();
            int[,] matrix = 
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };
            int[,] expectedMatrix =
            {
                { 7, 4, 1 },
                { 8, 5, 2 },
                { 9, 6, 3 }
            };

            matrix = rotator.Rotate(matrix);

            Assert.Equal(expectedMatrix, matrix);
        }

        [Fact]
        public void DontMatrix()
        {
            MatrixRotator rotator = new MatrixRotator();
            int[,] matrix =
            {
                { 1, 2, 3 },
                { 4, 5, 6 }
            };
            Assert.Throws<ArgumentException>(() => rotator.Rotate(matrix));
        }
    }
}
