﻿using MoneyveoTest.Processors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MoneyveoTest.UnitTests
{
    public class CsvWriterTest
    {
        [Fact]
        public async Task SaveCsv()
        {
            string fileName = "unittest.csv";
            int[,] matrix = { { 1, 2 }, { 3, 4 } };
            string expectedText = "1,2\n3,4";

            CsvWriter csvWriter = new CsvWriter();

            await csvWriter.WriteAsync(fileName, matrix);

            var actualText = File.ReadAllText(fileName);
            Assert.Equal(expectedText, actualText);

            File.Delete(fileName);
        }
    }
}
