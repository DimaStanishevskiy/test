using MoneyveoTest.Processors;
using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace MoneyveoTest.UnitTests
{
    public class CsvReaderTest
    {
        [Fact]
        public async Task CorrectText()
        {
            string fileName = "unittest.csv";
            string textCsv = "1,2\n3,4";
            File.WriteAllText(fileName, textCsv);
            CsvReader reader = new CsvReader();

            var result = await reader.ReadAsync(fileName);
            Assert.Equal(new int[,] { { 1, 2 }, { 3, 4 } }, result);

            File.Delete(fileName);
        }

        [Fact]
        public async Task IncorrectText()
        {
            string fileName = "unittest.csv";
            string textCsv = "qwerer";
            File.WriteAllText(fileName, textCsv);
            CsvReader reader = new CsvReader();

            await Assert.ThrowsAnyAsync<Exception>(() => reader.ReadAsync(fileName));

            File.Delete(fileName);
        }
    }
}
