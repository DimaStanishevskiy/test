﻿using MoneyveoTest.Processors;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MoneyveoTest.UnitTests
{
    public class MatrixGeneratorTest
    {
        [Fact]
        public void CheckSize()
        {
            int expectedSize = 10;
            MatrixGenerator generator = new MatrixGenerator();

            var matrix = generator.Generate(expectedSize);

            Assert.Equal(expectedSize, matrix.GetLength(0));
            Assert.Equal(expectedSize, matrix.GetLength(1));
        }

        [Fact]
        public void DontDuplicated()
        {
            int size = 10;
            MatrixGenerator generator = new MatrixGenerator();

            var firstMatrix = generator.Generate(size);
            var secondMatrix = generator.Generate(size);

            Assert.NotEqual(firstMatrix, secondMatrix);

        }

        [Fact]
        public void NegativeSize()
        {
            int size = -1;
            MatrixGenerator generator = new MatrixGenerator();
            
            Assert.Throws<ArgumentException>(() => generator.Generate(size));
        }
    }
}
